import Image from "next/image";
import Link from "next/link";
import { SiteConfig as configs } from "~/utils/site-configs";
import HamburgerMenu from "./HamburgerMenu";
import { signIn, signOut, useSession } from "next-auth/react";
import * as Avatar from "@radix-ui/react-avatar";

export const UserAvatar = ({
  name,
  profileUrl,
}: {
  name: string;
  profileUrl: string;
}) => {
  return (
    <Avatar.Root className="inline-flex h-[2rem] w-[2rem] select-none items-center justify-center overflow-hidden rounded-full bg-blackA3 align-middle">
      <Avatar.Image
        className="h-full w-full rounded-[inherit] object-cover"
        src={profileUrl}
        alt={`Profile pic of ${name}`}
      />
      <Avatar.Fallback
        className="leading-1 flex h-full w-full items-center justify-center bg-white text-[15px] font-medium text-violet11"
        delayMs={600}
      >
        {name.split(" ").map((e) => e.at(1))}
      </Avatar.Fallback>
    </Avatar.Root>
  );
};

export type NavbarMenuProps = {
  name: string;
  url: string;
  submenus?: NavbarMenuChildProps[];
};
export type NavbarMenuChildProps = {
  name: string;
  url: string;
};

export function Navbar() {
  const { data: sessionData } = useSession();

  const handleAuth = () => {
    sessionData ? void signOut() : void signIn();
  };

  const menus: NavbarMenuProps[] = [
    {
      name: "Jelajahi",
      url: "#",
      submenus: [
        {
          name: "Cari dari Lokasi",
          url: "#",
        },
        {
          name: "Cari dari Postingan",
          url: "#",
        },
      ],
    },
    {
      name: "Blog",
      url: "#",
    },
    {
      name: "Tentang Kami",
      url: "#",
    },
    {
      name: "Donasi",
      url: "#",
    },
    {
      name: "Masuk",
      url: "#",
    },
  ];
  return (
    <>
      <nav className="fixed w-full border-gray-200 bg-white px-2 dark:border-gray-700 dark:bg-gray-900">
        <div className="container mx-auto flex flex-wrap items-center justify-between">
          <div className="block md:hidden">
            <HamburgerMenu items={menus} />
          </div>
          <a href={configs.url} className="grow md:grow-0">
            <Image
              src={configs.logo}
              className="mx-auto h-10 md:mx-2"
              alt="Itamtom Logo"
              width={120}
              height={40}
            />
          </a>
          <div
            className="hidden w-full md:block md:w-auto"
            id="navbar-dropdown"
          >
            <ul className="mt-4 flex flex-col rounded-lg border border-gray-100 bg-gray-50 p-4 dark:border-gray-700 dark:bg-gray-800 md:mt-0 md:flex-row md:space-x-8 md:border-0 md:bg-white md:text-sm md:font-medium md:dark:bg-gray-900">
              {menus.map((menu, index) => (
                <li key={index}>
                  <div className="flex items-center gap-1">
                    <Link
                      href={menu.url}
                      className="block rounded-full px-4 py-2 text-slate-900 hover:bg-gray-100 hover:text-purple-500 dark:text-white dark:hover:bg-gray-600 dark:hover:text-white"
                      onClick={
                        index === menus.length - 1
                          ? () => handleAuth()
                          : () => {}
                      }
                    >
                      {sessionData && index === menus.length - 1
                        ? sessionData.user.name
                        : menu.name}
                    </Link>
                    {sessionData?.user.image && index === menus.length - 1 && (
                      <Link
                        href={menu.url}
                        onClick={
                          index === menus.length - 1
                            ? () => handleAuth()
                            : () => {}
                        }
                      >
                        <UserAvatar
                          profileUrl={sessionData?.user.image}
                          name={sessionData?.user.name ?? ""}
                        />
                      </Link>
                    )}
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
