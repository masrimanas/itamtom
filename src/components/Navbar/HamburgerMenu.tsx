import React, { useEffect } from "react";
import * as DropdownMenu from "@radix-ui/react-dropdown-menu";
import {
  HamburgerMenuIcon,
  CheckIcon,
  ChevronRightIcon,
} from "@radix-ui/react-icons";
import { type NavbarMenuProps } from "./Navbar";
import { useTheme } from "next-themes";

const HamburgerMenu = ({ items }: { items: NavbarMenuProps[] }) => {
  const {
    systemTheme,
    resolvedTheme: systemThemeName,
    theme,
    setTheme,
  } = useTheme();

  const getHamburgerColor = (): string => {
    switch (theme) {
      // case "dark":
      //   return "white";
      // case "light":
      //   return "black";
      // case "system":
      //   return systemTheme === "dark" ? "white" : "black";
      default:
        return 'purple';
    }
  };

  const handleTheme = () => {
    switch (systemThemeName) {
      case "dark":
        setTheme("light");
        break;
      case "light":
        setTheme("dark");
        break;
      default:
        systemTheme === "dark" ? setTheme("light") : setTheme("dark");
        break;
    }
  };

  return (
    <DropdownMenu.Root>
      <DropdownMenu.Trigger asChild>
        <button
          className="inline-flex h-[35px] w-[35px] items-center justify-center text-violet11 outline-none"
          aria-label="Customise options"
        >
          <HamburgerMenuIcon color={getHamburgerColor()} />
        </button>
      </DropdownMenu.Trigger>

      <DropdownMenu.Portal>
        <DropdownMenu.Content
          className="min-w-[10rem] rounded-md bg-white p-[5px] shadow-[0px_10px_38px_-10px_rgba(22,_23,_24,_0.35),_0px_10px_20px_-15px_rgba(22,_23,_24,_0.2)] will-change-[opacity,transform] data-[side=top]:animate-slideDownAndFade data-[side=right]:animate-slideLeftAndFade data-[side=bottom]:animate-slideUpAndFade data-[side=left]:animate-slideRightAndFade dark:bg-gray-800"
          sideOffset={5}
        >
          {items.map((item, index) =>
            item.submenus ? (
              <DropdownMenu.Sub key={index}>
                <DropdownMenu.SubTrigger className="group relative flex h-[25px] select-none items-center rounded-[3px] px-[5px] pl-[25px] text-[13px] leading-none text-violet11 outline-none data-[disabled]:pointer-events-none data-[state=open]:bg-violet4 data-[highlighted]:bg-violet9 data-[highlighted]:data-[state=open]:bg-violet9 data-[state=open]:text-violet11 data-[disabled]:text-mauve8 data-[highlighted]:text-violet1 data-[highlighted]:data-[state=open]:text-violet1">
                  {item.name}
                  <div className="ml-auto pl-[20px] text-mauve11 group-data-[disabled]:text-mauve8 group-data-[highlighted]:text-white">
                    <ChevronRightIcon />
                  </div>
                </DropdownMenu.SubTrigger>
                <DropdownMenu.Portal>
                  <DropdownMenu.SubContent
                    className="min-w-[220px] rounded-md bg-white p-[5px] shadow-[0px_10px_38px_-10px_rgba(22,_23,_24,_0.35),_0px_10px_20px_-15px_rgba(22,_23,_24,_0.2)] will-change-[opacity,transform] data-[side=top]:animate-slideDownAndFade data-[side=right]:animate-slideLeftAndFade data-[side=bottom]:animate-slideUpAndFade data-[side=left]:animate-slideRightAndFade dark:bg-gray-800"
                    sideOffset={2}
                    alignOffset={-5}
                  >
                    {item.submenus.map((subitem, index) => (
                      <DropdownMenu.Item
                        key={index}
                        className="group relative flex h-[25px] select-none items-center rounded-[3px] px-[5px] pl-[25px] text-[13px] leading-none text-violet11 outline-none data-[disabled]:pointer-events-none data-[highlighted]:bg-violet9 data-[disabled]:text-mauve8 data-[highlighted]:text-violet1"
                      >
                        {subitem.name}
                        <div className="ml-auto pl-[20px] text-white group-data-[disabled]:text-mauve8 group-data-[highlighted]:text-white"></div>
                      </DropdownMenu.Item>
                    ))}
                  </DropdownMenu.SubContent>
                </DropdownMenu.Portal>
              </DropdownMenu.Sub>
            ) : (
              <DropdownMenu.Item
                key={index}
                className="group relative flex h-[25px] select-none items-center rounded-[3px] px-[5px] pl-[25px] text-[13px] leading-none text-violet11 outline-none data-[disabled]:pointer-events-none data-[highlighted]:bg-violet9 data-[disabled]:text-mauve8 data-[highlighted]:text-violet1"
              >
                {item.name}
                <div className="ml-auto pl-[20px] text-white group-data-[disabled]:text-mauve8 group-data-[highlighted]:text-white"></div>
              </DropdownMenu.Item>
            )
          )}
          <DropdownMenu.Separator className="m-[5px] h-[1px] bg-violet6" />
          <DropdownMenu.CheckboxItem
            className="group relative flex h-[25px] select-none items-center rounded-[3px] px-[5px] pl-[25px] text-[13px] leading-none text-violet11 outline-none data-[disabled]:pointer-events-none data-[highlighted]:bg-violet9 data-[disabled]:text-mauve8 data-[highlighted]:text-violet1"
            checked={systemThemeName === "dark"}
            onCheckedChange={() => handleTheme()}
          >
            <DropdownMenu.ItemIndicator className="absolute left-0 inline-flex w-[25px] items-center justify-center">
              <CheckIcon />
            </DropdownMenu.ItemIndicator>
            Dark Mode{" "}
            <div className="ml-auto pl-[20px] text-mauve11 group-data-[disabled]:text-mauve8 group-data-[highlighted]:text-white"></div>
          </DropdownMenu.CheckboxItem>
        </DropdownMenu.Content>
      </DropdownMenu.Portal>
    </DropdownMenu.Root>
  );
};

export default HamburgerMenu;
