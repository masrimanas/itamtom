

export const SiteConfig = {
  siteName: "Itamtom",
  title: "Itamtom",
  logo: "/itamtom-logo.svg",
  url: "http://itamtom.com/",
  description: "Find your anabul",
  locale: "id",
  language: "en",
  devUrl: "http://localhost:3000",
};

export type SiteConfig = typeof SiteConfig;

